/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
		
		var viewInicio = Vue.component('view-inicio', {
		template: '#view-inicio',
		data: function () {
		return { message: 'Libro Interactivo' }
		}
		});
		var viewInformacion = Vue.component('view-informacion', {
		template: '#view-informacion',
		data: function () {
		return { message: 'App realizada por Salinas & Ortega' }
		}
		});
		var viewImagen1 = Vue.component('view-imagen1', {
		template: '#view-imagen1',
		data: function () {
		return { imagen01: './img/dibujo01.jpg' }
		}
		});
		var viewImagen2 = Vue.component('view-imagen2', {
		template: '#view-imagen2',
		data: function () {
		return { imagen02: './img/dibujo02.jpg' }
		}
		});
		var viewImagen3 = Vue.component('view-imagen3', {
		template: '#view-imagen3',
		data: function () {
		return { imagen03: './img/dibujo03.jpg' }
		}
		});
		var viewImagen4 = Vue.component('view-imagen4', {
		template: '#view-imagen4',
		data: function () {
		return { imagen04: './img/dibujo04.jpg' }
		}
		});
		var viewImagen5 = Vue.component('view-imagen5', {
		template: '#view-imagen5',
		data: function () {
		return { imagen05: './img/dibujo05.jpg' }
		}
		});

		var router = new VueRouter({
		routes: [
		{ path: '/inicio', component: viewInicio },
		{ path: '/informacion', component: viewInformacion },
		{ path: '/imagen1', component: viewImagen1 },
		{ path: '/imagen2', component: viewImagen2 },
		{ path: '/imagen3', component: viewImagen3 },
		{ path: '/imagen4', component: viewImagen4 },
		{ path: '/imagen5', component: viewImagen5 }
		]
		});
		var vm = new Vue({
		el: '#app',
		data: {
		currentRoute: window.location.pathname
		},
		router: router
		});
		
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

app.initialize();